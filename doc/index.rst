An ILAMB Prototype
==================

The International Land Model Benchmarking (ILAMB) project is a model-data intercomparison and integration project designed to improve the performance of land models and, in parallel, improve the design of new measurement campaigns to reduce uncertainties associated with key land surface processes. Building upon past model evaluation studies, the goals of ILAMB are to:

* develop internationally accepted benchmarks for land model performance, 
* promote the use of these benchmarks by the international community for model intercomparison, 
* strengthen linkages between experimental, remote sensing, and climate modeling communities in the design of new model tests and new measurement programs, and 
* support the design and development of a new, open source, benchmarking software system for use by the international community.

We are in the process of a redesign of the current codebase_. The following links lead to pages which explain and document our design philosophy and current progress.

.. _codebase: https://bitbucket.org/ncollier/ilamb

.. toctree::
   :maxdepth: 1

   PACKAGEAPI
   TUTORIAL
   RESULTS
